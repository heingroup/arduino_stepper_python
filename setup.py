import arduino_stepper.version as version
from setuptools import setup, find_packages

setup(
    name='Arduino Stepper',
    version=version.__version__,
    packages=find_packages(),
    url='https://gitlab.com/heingroup/arduino_stepper_python',
    author='Henry Situ // Hein Group',
    description='Python API the Arduino stepper controller',
    install_requires=[
        'pyserial>=3.4',
    ],
)