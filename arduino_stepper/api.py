import serial
import logging
import time


class ArduinoStepper(object):
    """
    A class to communicate with an Arduino stepper motor controller.
    Serial commands
    SCR: Set CCR
    SPS: Set position in steps
    SIR: Set initial comp register value for acceleration
    SHR: Set the home CCR value
    SHD: Sets the home direction
    QCP: Query for current position relative to target position
    QTP: Query for target position
    QCR: Query for the CCR value
    QIR: Query for initial comp register value for acceleration
    STP: Stops the motor
    HME: Rotates stepper motor until limit switch is pressed

    Responses
    -1: good
    -2: error in command
    -3: busy
    -4: done action
    """
    def __init__(self,
                 motor_resolution,
                 com_port,
                 baudrate,
                 timer_prescaler=1,
                 default_ms=2
                 ):
        self.cw = 0
        self.ccw = 1
        self.motor_res = motor_resolution   # the motor resolution in step/rev when full stepping
        self.motorspeed: int = 200    # in steps/s (=counts/s)
        self._init_register: int = 65222     # the initial comp register value used for acceleration
        self._comp_register: int = 26667     # the default comp register value used for coasting speed
        self._home_register: int = 20000     # the CCR value used for homing
        self._ms = default_ms   # the microstep value
        self._comport = com_port
        self._baudrate = baudrate
        self._prescaler = timer_prescaler * 2   # Double the duty cycle. Half goes to high. Half goes to low.
        self.clk_freq = 16000000    # the clock frequency of an Arduino Uno
        self._max_comp_register = 2**16 - 1
        self.logger = logging.getLogger(f'{self.__class__.__name__}')
        self._relay = True
        self._connect()
        response = self._ser.read_until()
        self.logger.debug(f'"{response}"')
        self.ping_accel_regsiter()
        self.ping_comp_register()

    def _connect(self):
        """Connect to the motor controller"""
        # serial connection
        self.logger.debug('initializing serial object')
        self._ser = serial.Serial(
            port=self.comport,
            baudrate=self.baudrate,
            parity=serial.PARITY_NONE,
            bytesize=serial.EIGHTBITS,
            stopbits=serial.STOPBITS_ONE,
        )
        # wait until "ready" is returned
        self.logger.debug('waiting for "ready"')
        self._ser.read_until()
        self.logger.debug('connected')

    def check_value(self, value: int, min_value: int):
        if value > self._max_comp_register:
            value = self._max_comp_register
        elif value < min_value:
            value = min_value

        return value

    def calc_travel_time(self, steps, speed):
        steps = abs(steps)
        accel_time = speed / self.acceleration_steps
        accel_dist = 0.5 * self.acceleration_steps * accel_time**2
        if accel_dist > steps/2:
            accel_dist = steps/2
            travel_time = 2*(accel_dist/(0.5 * self.acceleration_steps))**(1/2)

        else:
            const_dist = steps - 2*accel_dist
            const_time = const_dist / speed
            travel_time = const_time + 2 * accel_time

        return travel_time

    @property
    def comport(self) -> str:
        return f'COM{self._comport}'

    @comport.setter
    def comport(self, value: int):
        self._comport = value

    @property
    def baudrate(self):
        return self._baudrate

    def send_string(self, string: str) -> str:
        """
        Sends the specified string on the serial IO

        :param string: sting to send
        :return: response
        """
        # append newline if not present
        self.logger.debug(f'sending "{string}"')
        if string.endswith('\r') is False:
            string = string + '\r'
        self._ser.write(string.encode())
        response = self._ser.read_until()
        self.logger.debug(f'received response: "{response}"')
        return response.decode()

    @property
    def motor_resolution(self) -> int:
        return self.motor_res   # steps/rev

    @property
    def microstep(self) -> int:
        return self._ms

    @property
    def accel_comp_register(self) -> int:
        return self._init_register

    @accel_comp_register.setter
    def accel_comp_register(self, value: int):
        value = self.check_value(value, min_value=1)
        cmd = 'SIR ' + str(value)
        self.send_string(cmd)
        self._init_register = value

    @property
    def acceleration_steps(self) -> float:
        """Acceleration in steps/s/s"""
        return 2*(0.676*self.clk_freq/self.accel_comp_register)**2

    @acceleration_steps.setter
    def acceleration_steps(self, value: float):
        """Sets the acceleration in steps/s/s"""
        if self.acceleration_steps_range[0] <= value <= self.acceleration_steps_range[1]:
            self.accel_comp_register = 0.676*self.clk_freq*(2/value)**(1/2)
        else:
            raise ValueError(f'The acceleration range is "{self.acceleration_steps_range[0]}" to '
                             f'"{self.acceleration_steps_range[1]}" steps/s/s.')

    @property
    def acceleration_steps_range(self) -> [float]:
        """Returns the min and max acceleration in steps/s/s"""
        return 2*(0.676*self.clk_freq/self._max_comp_register)**2, 2*(0.676*self.clk_freq)**2

    @property
    def home_register(self) -> int:
        """The CCR for homing"""
        return self._home_register

    @home_register.setter
    def home_register(self, value: int):
        value = self.check_value(value, min_value=1)
        cmd = 'SHR ' + str(value)
        self.send_string(cmd)
        self._home_register = value

    @property
    def comp_register(self) -> int:
        return self._comp_register

    @comp_register.setter
    def comp_register(self, value: int):
        value = self.check_value(value, min_value=1)
        self._comp_register = value

    @property
    def motor_speed_steps(self) -> float:
        """The current motor speed in steps/s."""
        return self.motorspeed

    @motor_speed_steps.setter
    def motor_speed_steps(self, motorspeed: float):
        self.motorspeed = motorspeed

        self.comp_register = int(round(self.clk_freq/(self._prescaler*self.motorspeed) - 1))
        cmd = 'SCR ' + str(self.comp_register)
        self.send_string(string=cmd)

    @property
    def motor_speed_rev(self) -> float:
        """The motor speed in rev/s."""
        return self.motor_speed_steps / (self._ms * self.motor_resolution)

    @motor_speed_rev.setter
    def motor_speed_rev(self, motorspeed_rev: float):
        self.motor_speed_steps = motorspeed_rev * self.motor_resolution * self._ms

    @property
    def relay(self) -> bool:
        return self._relay

    @relay.setter
    def relay(self, state: bool):
        self._relay = state
        if state is False:
            val = 0
        else:
            val = 1
        cmd = 'RLY ' + str(val)
        self.send_string(string=cmd)

    def rotate_steps(self, steps: int, speed: int = None, block: bool = False):
        """Rotates the motor to the desired number of steps and speed.

        :param steps: the number of steps to rotate
        :param speed: the speed of the motor in steps/s
        :param block: Makes the function blocking
        """
        if speed is not None and speed != self.motor_speed_steps:
            speed = abs(speed)
            self.motor_speed_steps = speed
        cmd = 'SPS ' + str(steps)
        self.send_string(string=cmd)
        if block:
            time.sleep(self.calc_travel_time(steps=steps, speed=speed))
            while block:
                reply = self._ser.read_until()
                reply = str(reply)
                if "-4" in reply:
                    break
                time.sleep(1)

    def rotate_revolution(self, rev, speed: float = None, block: bool = False):
        """Rotates the motor to the desired revolutions at speed.

        :param rev: the number of revolutions to rotate
        :param speed: the speed in rev/s
        :param block: Makes the function blocking
        :return:
        """
        speed_steps = None
        if speed is not None:
            speed_steps = speed * self.motor_resolution * self._ms
        steps = rev * self.motor_resolution * self._ms
        self.rotate_steps(steps=steps, speed=speed_steps, block=block)

    def home(self, block: bool = True):
        """Rotates stepper motor until limit switch is pressed"""
        cmd = "HME "
        response = self.send_string(string=cmd)
        while block:
            time.sleep(1)
            reply = self._ser.read_until()
            reply = str(reply)
            if "-4" in reply or response:
                break
        return response

    def home_direction(self, direction):
        if direction != 1 and direction != 0:
            raise ValueError('Direction needs to be 1 for ccw or 0 for cw.')
        cmd = "SHD" + str(direction)
        response = self.send_string(string=cmd)
        return response

    def current_position(self):
        """Pings that Arduino for the current position of the motor while rotating."""
        current_position = self.send_string(string='QCP')
        return current_position

    def target_position(self):
        """Pings that Arduino for the target position of the motor while rotating."""
        target_position = self.send_string(string='QTP')
        return target_position

    def busy(self):
        """Checks to see if the motor is busy."""
        response = self.send_string(string='BSY')
        return response

    def stop_motor(self):
        """Stops the motor"""
        response = self.send_string(string='STP')
        return response

    def ping_accel_regsiter(self):
        response = self.send_string(string='QIR')
        self.accel_comp_register = int(response)
        return response

    def ping_comp_register(self):
        response = self.send_string(string='QCR')
        self.comp_register = int(response)
        return response
